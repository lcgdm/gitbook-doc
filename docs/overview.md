Overview
============

The Disk Pool Manager (DPM) is a lightweight storage solution for grid sites. It offers a simple way to create a disk-based grid storage element and supports relevant protocols (SRM, gridFTP, ROOT, HTTP/DAV) for file management and access.
It focus on manageability (ease of installation, configuration, low effort of maintenance), while providing all required functionality for a grid storage solution (support for multiple disk server nodes, different space types, multiple file replicas in disk pools).

The LCG File catalogue (LFC) offers a hierarchical view of files to users, with a UNIX-like client interface. The LFC catalogue provides:

* Logical File Name (LFN) to Storage URL (SURL) mappings (via a GUID);
* Authorisation on its logical namespace.
